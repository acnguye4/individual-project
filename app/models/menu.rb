class Menu < ActiveRecord::Base
  attr_accessible :food_name, :price

   has_many :tickets
  has_many :ticket_infos, :through => :tickets
end
