class Ticket < ActiveRecord::Base
  attr_accessible :cooking, :food_name, :in_queue, :paid, :price, :quantity, :served, :subtotal, :tax, :total

  belongs_to :ticket_info
  belongs_to :menu

  before_save do
    total
  end
  def subtotal
    self.subtotal = self.price * self.quantity
  end

   def tax
     self.tax = self.subtotal * 0.0825
   end

  def total
    self.total = self.subtotal + self.tax
  end
end
