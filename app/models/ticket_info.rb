class TicketInfo < ActiveRecord::Base
  attr_accessible :cook, :guest_number, :table_number, :ticket_id, :waiter

  has_many :tickets
  has_many :menus, :through => :tickets
end
