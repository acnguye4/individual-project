class TicketInfosController < ApplicationController
  # GET /ticket_infos
  # GET /ticket_infos.json
  def index
    @ticket_infos = TicketInfo.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @ticket_infos }
    end
  end

  # GET /ticket_infos/1
  # GET /ticket_infos/1.json
  def show
    @ticket_info = TicketInfo.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @ticket_info }
    end
  end

  # GET /ticket_infos/new
  # GET /ticket_infos/new.json
  def new
    @ticket_info = TicketInfo.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @ticket_info }
    end
  end

  # GET /ticket_infos/1/edit
  def edit
    @ticket_info = TicketInfo.find(params[:id])
  end

  # POST /ticket_infos
  # POST /ticket_infos.json
  def create
    @ticket_info = TicketInfo.new(params[:ticket_info])

    respond_to do |format|
      if @ticket_info.save
        format.html { redirect_to @ticket_info, notice: 'Ticket info was successfully created.' }
        format.json { render json: @ticket_info, status: :created, location: @ticket_info }
      else
        format.html { render action: "new" }
        format.json { render json: @ticket_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /ticket_infos/1
  # PUT /ticket_infos/1.json
  def update
    @ticket_info = TicketInfo.find(params[:id])

    respond_to do |format|
      if @ticket_info.update_attributes(params[:ticket_info])
        format.html { redirect_to @ticket_info, notice: 'Ticket info was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @ticket_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ticket_infos/1
  # DELETE /ticket_infos/1.json
  def destroy
    @ticket_info = TicketInfo.find(params[:id])
    @ticket_info.destroy

    respond_to do |format|
      format.html { redirect_to ticket_infos_url }
      format.json { head :no_content }
    end
  end
end
