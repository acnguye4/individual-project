# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20131106034833) do

  create_table "menus", :force => true do |t|
    t.string   "food_name"
    t.float    "price"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "ticket_infos", :force => true do |t|
    t.integer  "ticket_id"
    t.integer  "table_number"
    t.integer  "guest_number"
    t.string   "waiter"
    t.string   "cook"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "tickets", :force => true do |t|
    t.string   "food_name"
    t.float    "price"
    t.integer  "quantity"
    t.float    "subtotal"
    t.float    "tax"
    t.float    "total"
    t.boolean  "in_queue"
    t.boolean  "cooking"
    t.boolean  "served"
    t.boolean  "paid"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
