class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :food_name
      t.float :price
      t.integer :quantity
      t.float :subtotal
      t.float :tax
      t.float :total
      t.boolean :in_queue
      t.boolean :cooking
      t.boolean :served
      t.boolean :paid

      t.timestamps
    end
  end
end
