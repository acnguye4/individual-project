class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.string :food_name
      t.float :price

      t.timestamps
    end
  end
end
