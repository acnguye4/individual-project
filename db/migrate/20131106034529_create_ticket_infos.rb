class CreateTicketInfos < ActiveRecord::Migration
  def change
    create_table :ticket_infos do |t|
      t.integer :ticket_id
      t.integer :table_number
      t.integer :guest_number
      t.string :waiter
      t.string :cook

      t.timestamps
    end
  end
end
