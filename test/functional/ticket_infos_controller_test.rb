require 'test_helper'

class TicketInfosControllerTest < ActionController::TestCase
  setup do
    @ticket_info = ticket_infos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ticket_infos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ticket_info" do
    assert_difference('TicketInfo.count') do
      post :create, ticket_info: { cook: @ticket_info.cook, guest_number: @ticket_info.guest_number, table_number: @ticket_info.table_number, ticket_id: @ticket_info.ticket_id, waiter: @ticket_info.waiter }
    end

    assert_redirected_to ticket_info_path(assigns(:ticket_info))
  end

  test "should show ticket_info" do
    get :show, id: @ticket_info
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ticket_info
    assert_response :success
  end

  test "should update ticket_info" do
    put :update, id: @ticket_info, ticket_info: { cook: @ticket_info.cook, guest_number: @ticket_info.guest_number, table_number: @ticket_info.table_number, ticket_id: @ticket_info.ticket_id, waiter: @ticket_info.waiter }
    assert_redirected_to ticket_info_path(assigns(:ticket_info))
  end

  test "should destroy ticket_info" do
    assert_difference('TicketInfo.count', -1) do
      delete :destroy, id: @ticket_info
    end

    assert_redirected_to ticket_infos_path
  end
end
