require 'test_helper'

class TicketsControllerTest < ActionController::TestCase
  setup do
    @ticket = tickets(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tickets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ticket" do
    assert_difference('Ticket.count') do
      post :create, ticket: { cooking: @ticket.cooking, food_name: @ticket.food_name, in_queue: @ticket.in_queue, paid: @ticket.paid, price: @ticket.price, quantity: @ticket.quantity, served: @ticket.served, subtotal: @ticket.subtotal, tax: @ticket.tax, total: @ticket.total }
    end

    assert_redirected_to ticket_path(assigns(:ticket))
  end

  test "should show ticket" do
    get :show, id: @ticket
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ticket
    assert_response :success
  end

  test "should update ticket" do
    put :update, id: @ticket, ticket: { cooking: @ticket.cooking, food_name: @ticket.food_name, in_queue: @ticket.in_queue, paid: @ticket.paid, price: @ticket.price, quantity: @ticket.quantity, served: @ticket.served, subtotal: @ticket.subtotal, tax: @ticket.tax, total: @ticket.total }
    assert_redirected_to ticket_path(assigns(:ticket))
  end

  test "should destroy ticket" do
    assert_difference('Ticket.count', -1) do
      delete :destroy, id: @ticket
    end

    assert_redirected_to tickets_path
  end
end
